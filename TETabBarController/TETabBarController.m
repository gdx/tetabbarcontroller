//
//  TETabBarController.m
//  AmazPhoto
//
//  Created by  on 12/3/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TETabBarController.h"

@interface TETabBarController ()

- (void)initViewControllerAndTabBarItems;

@end

@implementation TETabBarController

@synthesize viewControllers = _viewControllers;
@synthesize tabBarItems = _tabBarItems;

#pragma mark - Private methods

- (void)initViewControllerAndTabBarItems {
    
}

#pragma mark - Public methods

- (void)setViewControllers:(NSArray *)viewControllers tabBarItems:(NSArray *)tabBarItems {
    
}

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
