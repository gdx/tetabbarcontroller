//
//  TETabBarController.h
//  AmazPhoto
//
//  Created by  on 12/3/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TETabBarController : UIViewController

@property (readonly, nonatomic) NSArray *viewControllers;
@property (readonly, nonatomic) NSArray *tabBarItems;

- (void)setViewControllers:(NSArray *)viewControllers tabBarItems:(NSArray *)tabBarItems;

@end
